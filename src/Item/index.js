import React from 'react';
import './Item.css'

function Item(props){

	return(
		<li className="Item">
      <span className={`Icon Icon-check ${props.completed && 'Icon-check--active'}`} onClick={props.onComplete}>
      √
      </span>
      <p className={`Item-p ${props.completed && 'Item-p--complete'}`}>
        {props.text}
      </p>
      <span className="Icon Icon-delete" onClick={props.onDelete}>
      <img className='imagen' src="https://cdn.iconscout.com/icon/premium/png-256-thumb/delete-1773351-1509666.png"/>
      </span>
    </li>
	);

}

export {Item};